open! Base

(* Ecrire une fonction imax e ls qio renvoie le maximum d'une liste d'enter ls positifs
 ou e si la liste est vide *)

let rec imax e ls =
    match ls with
    | [] -> e
    | a :: b -> 
    if a < e 
        then imax e b 
    else 
        imax a b;;

let%test "Testing imax..." =
  [%compare.equal: int] (imax (-1) [11;15;7;34;15]) (34)
;;

(* Écrire une fonction omax ls qui renvoie une option sur le maximum de la liste d’entiers ls.
 En d’autres termes s'ils est vide, omax ls renvoie None et s'ils est non-vide avec comme 
 valeur maximumm, alorsomax ls renvoie Some m *)

let omax ls =
   match ls with
   | [] -> None
   | a :: b -> Some (imax (-1) ls);;

let%test "Testing imax..." =
  match omax [11;15;7;34;15] with
    | Some 34 -> true
    | Some _ -> false
    | None -> false
;;

let%test "Testing imax..." =
  match omax [] with
    | Some _ -> false
    | None -> true
;;

(* Reprendre les questions précédentes mais pour des types quelconques en ajoutant aux paramètres
 une fonction de comparaison.
 Pour un type ’a une fonction cmp est une fonction de comparaison si elle a le type ’a-> ’a -> int
 et qu’elle renvoie un nombre négatif si le premier paramètre est plus petit que le second,
  un nombre positif si le premier paramètre est plus grand que le second, et zéro en cas d’égalité. *)


(* Écrire une fonction dotprod xs ys qui calcule le produit scalaire de deux vecteurs xs ys représentés
 par des listes d’entiers.
 Généraliser ensuite pour prendre en compte d’autres types de nombres.
 Comparer letype de cette dernière fonction donné parl’interpréteur et celui que vous leur attribuez. *)

(* Il faut que xs, ys soit de meme longueur pour faire la multiplication; du coup on utilise map2_exn *)
let dotprod xs ys =
    List.fold_left ~f:(+) ~init:0 (List.map2_exn ~f:( * ) xs ys);;

let%test "Testing dotprod..." =
  [%compare.equal: int] (dotprod [2;3;5] [12;20;1]) (89)
;;

(* Implémenter la concaténation de deux listes append l1 l2qui renvoie la liste constituée des élements de l1
puis de ceux de l2 *)

let append l1 l2 =
    List.append l1 l2;;

let rec append_by_hand l1 l2 =
    match l1 with
    | [] -> l2
    | a :: b -> a :: (append_by_hand b l2)
;;

let%test "Testing append..." =
  [%compare.equal: int list] (append [2;3;5] [12;20;1]) ([2;3;5;12;20;1])
;;

let%test "Testing append..." =
  [%compare.equal: int list] (append_by_hand [2;3;5] [12;20;1]) ([2;3;5;12;20;1])
;;

(* Écrire la fonction rev_append l1 l2 quirenvoie la liste constituée des éléments del1du dernier au premier
 puis de ceux de l2 dans l’ordre *)

let rev_append l1 l2 =
    List.rev_append l1 l2;;

let rec rev_append_by_hand l1 l2 =
    match l1 with
    | [] -> l2
    | a :: b -> rev_append_by_hand b (a :: l2)

let%test "Testing append..." =
  [%compare.equal: int list] (rev_append_by_hand [2;3;5] [12;20;1]) ([5;3;2;12;20;1])