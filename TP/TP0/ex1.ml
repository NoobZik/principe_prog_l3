open! Base

(* Écrire une fonction split n ls qui étant donné un entier n, appelé pivot, et une liste
 ls renvoie une paire de listes (l1, l2) contenant respectivement les éléments de ls plus
  petits que le pivot et les éléments de ls plus grands ou égaux.
  Indication : On peut faire une fonction split_aux qui a deux arguments supplémentaires 
  acc1 et acc2, deux listes qui servent d’accumulateurs pour construire l1 et l2. *)

(* version aux
let rec split_aux n ls l1 l2 =
    match ls with
    | [] -> (l1, l2)
    | a :: b -> 
        if (a > n) then
            split_aux n b l1 (a :: l2)
        else
            split_aux n b (a :: l1) l2
;;

let split n ls =
    split_aux n ls [] []
;;
*)

(* version sans aux *)

let rec split n ls =
    match ls with
    | [] -> ([], [])
    | a :: b -> let (l1, l2) = split n b in
        if (a < n) then
            a :: l1, l2
        else
            l1, a :: l2
;;
(* 
En utilisant la fonction split, écrire une fonction récursive quicksort qui
 trie une liste en utilisant la procédure suivante :
 • Si la liste est vide, rien à trier.
 • Sinon, en prenant la tête de la liste hd comme pivot pour découper en deux
    parties t1 et t2 la queue de la liste,
 • on utilise des appels récursifs à la fonction quicksort pour trier t1 et t2.
 • Finalement, on recompose la liste triée en utilisant les résultats du point
    précédent et le pivot. 
Indication : utiliser @ pour recoller des listes.
*)

(* version 1 *)
(*
let rec quicksort l =
    match l with
    | [] -> []
    | a :: b -> match (split a b) with
        | ([], []) -> [a]
        | (l1, l2) ->  (quicksort l1) @ a :: [] @ (quicksort l2)
;;
*)
(* version 2 *)
let rec quicksort l =
    match l with
    | [] -> []
    | a :: b -> let (l1, l2) = split a b in
        quicksort l1 @ [a] @ quicksort l2
;;

let li = [640; 5; 177; 123; 863; 137; 250; 262; 747; 709; 290; 328];;

let%test "Testing quicksort..." =
    [%compare.equal : int list] (quicksort li) ([5; 123; 137; 177; 250; 262; 290; 328; 640; 709; 747; 863])
;;