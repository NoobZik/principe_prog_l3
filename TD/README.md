# Principe de programmation #

* **Exercice 0**

*   Écrire une fonction imax e ls qui renvoie le maximum d’une liste d’entiers ls positifs ou e si la liste est vide.  (on supposera e négatif)

```ocaml
let rec imax e ls =
  match ls with
  | [] -> e
  | a :: suite -> let u = max x suite if u > e x then u else e

(* La version attendu *)

let rec imax e ls =
  match ls with
  | [] -> e
  | x :: suite -> if e > x then imax e suite else imax x suite
```

*   Écrire une fonction omax ls qui renvoie une option sur le maximum de la liste d’entiers ls. En d’autres termes s'il sest vide,omax ls renvoie Non e et s'il s'est non-vide avec comme valeur maximumm, alors omax ls renvoie Some m

```ocaml
let omax ls
  match ls with
  | [] -> raise "error"
  | x :: suite -> imax x suite
```

Correction

```ocaml
let omax ls =
  imax -1 ls

(* Ou *)
let omax ls =
  | [] -> -1 (* Ou raise "erreur" Ou None *)
  | x :: suite -> Some (imax x suite)
```


*  Implémenter la concaténation de deux listes append l1 l2 qui renvoie la liste constituée des élements de l1 puis de ceux de l2

```ocaml
let rec append l1 l2 =
  match l1 with
  | [] -> l2
  | x :: suite -> x :: (append suite l2)
```

*   Écrire la fonction rev_append l1 l2 qui renvoie la liste constituée des éléments de l1 du dernier au premier puis de ceux de l2 dans l’ordre

Exemple : rev_append [1,2,3] [4,5] >> [3,2,1,4,5]
*Brouillon*

```ocaml
let rec rev_append l1 l2 =
  match l1 with
  | [] -> l2
  | x :: suite -> suite :: (rev_append x l2)
```

*Correction, solution 1*

```ocaml
let rev_append l1 l2 =
  append (rev [] l1) l2

let rec rev acc l1 =
  match l1 with
  | [] -> acc
  | x :: suite -> rev (x :: acc) suite
```

*Solution 2*

```ocaml
let rec rev_append l1 l2 =
  match l1 with
  | [] -> l2
  | x :: suite -> rev_append suite (x :: l2)
```

*   Écrire une fonction dotprod xs ys qui calcule le produit scalaire de deux vecteurs xs ys représentés par des listes d’entiers.
Exemple : dotprod [2,3,5] [12,20,1] >> 24+60+5

*   Généraliser ensuite pour prendre en compte d’autres types de nombres.  
*   Comparer le type de cette dernière fonction donné par l’interpréteur et celui que vous leur attribuez.


```ocaml
let rec dotprod l1 l2 =
  match (l1 l2) with
  ([] , []) -> 0
  (x :: l1b, y :: l2b) -> x * y + dotprod(l1b l2b)
  ([] , y :: l2b) -> raise "Mon erreur"
  (x :: l1b, []) -> raise "Mon erreur"
```

* Reprendre les questions précédentes maispour des types quelconques en ajoutant auxparamètres une fonction de comparaison.Pour un type’aune fonctioncmpest unefonction de comparaison si elle a le type’a-> ’a -> intet qu’elle renvoie un nombrenégatif si le premier paramètre est plus petitque le second, un nombre positif si le pre-mier paramètre est plus grand que le second,et zéro en cas d’égalité

```ocaml

```


Exercice 2 :Écrire la fonctionval find :  (int -> bool)-> int list -> intqui renvoie le premier élé-ment d’une listeld’entiers qui vérifie un prédi-catp, s’il y en a, ou 0 sip(x)est faux pour toutélément del.

```ocaml
let rec find pred l = match l with
| [] -> 0
| x :: suite -> if pred x then x else find pred suite
```

Ecrire une fontion map_int : (int -> int) -> int list -> int list
tel que (mapint f l) applique la fonction f a tous les elements de l
